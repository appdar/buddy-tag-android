package com.appdar.buddytag.buddytag;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appdar.buddytag.buddytag.model.User;
import com.appdar.buddytag.buddytag.model.UserLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pubnub.api.Pubnub;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sj.singh on 3/14/14.
 */

public class MainFragment extends Fragment implements PubnubStore.PubnubStoreListener, LocationListener {
    final static String TAG = "MainFragment";

    PubnubStore pubnubStore;
    Map<Marker, UserLocation> markers = new HashMap<Marker, UserLocation>();

    private GoogleMap map;
    private LocationManager locationManager;
    private String provider;

    public MainFragment() {
        pubnubStore = new PubnubStore();
        pubnubStore.connect(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

        pubnubStore.setContext(getActivity());
        if (AppSession.getInstance(getActivity()).getUser() == null){
            showNameEntryDialog();
        }

        View v = inflater.inflate(R.layout.fragment_main, container, false);

        try {
            MapsInitializer.initialize(getActivity());
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        //Make sure the device has the latest google play services
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) )
        {
            case ConnectionResult.SUCCESS:
                break;
            case ConnectionResult.SERVICE_MISSING:
                GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), getActivity(), 1122).show();
                Toast.makeText(getActivity(), "SERVICE IS MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), getActivity(), 1122).show();
                Toast.makeText(getActivity(), "SERVICE UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default: Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }



        //Setup the map
        SupportMapFragment supportMapFragment = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.map1);
        map = supportMapFragment.getMap();
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setMyLocationEnabled(true);

        setupMapMarkerClick();


        setupLocationManager();

        return v;

    }

    //Pubnub Store interface methods
    @Override
    public void onLocationUpdate(final UserLocation userLocation){
        //Loop through current markers
        //If exists, update lat/lng
        //if doesn't, add to marker table and map

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Boolean isFound = false;

                for (Map.Entry entry : markers.entrySet()) {
                    Marker entryMarker = (Marker)entry.getKey();
                    UserLocation entryUserLocation = (UserLocation)entry.getValue();

                    if (entryUserLocation.getLocationUser().getUserId().equals(userLocation.getLocationUser().getUserId())){
                        updateUserLocation(entryMarker, userLocation);
                        entryUserLocation.setLocationLatLng(userLocation.getLocationLatLng());
                        isFound = true;
                        break;
                    }
                }

                if (!isFound){
                    addUserToMap(userLocation);
                }
            }
        });



    }

    @Override
    public void onUserTagged(final User taggedBy, final String tagMessage){
        //Run on main thread
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            Toast.makeText(getActivity(),"You have been tagged by " + taggedBy.getUserDisplayName() + "\r\nTag Message:" + tagMessage, 600).show();
            }
        });

    }

    //Location Manager interface methods
    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        if (AppSession.getInstance(getActivity()).getUser() != null){
            pubnubStore.sendUserLocation(lat, lng);
        }
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        Log.d(TAG, "Location status changed");
    }

    @Override
    public void onProviderDisabled(String arg0) {
        Toast.makeText(getActivity(), "Please enable GPS to share your location with your friends.",600).show();
    }
    @Override
    public void onProviderEnabled(String arg0) {
        locationManager.requestLocationUpdates(provider, 400, 1, (android.location.LocationListener) this);
    }

    //Marker methods
    private void addUserToMap(final UserLocation userLocation){

        Marker marker = map.addMarker(new MarkerOptions()
                .position(userLocation.getLocationLatLng())
                .draggable(true)
                .flat(true));
        markers.put(marker, userLocation);

    }

    private void updateUserLocation(final Marker marker,final UserLocation userLocation){
        marker.setPosition(userLocation.getLocationLatLng());
    }

    //Misc Private methods
    private void setupLocationManager(){
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location currentLocation = locationManager.getLastKnownLocation(provider);

        if (currentLocation != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(currentLocation);
            LatLng location = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location,10));
        }


        locationManager.requestLocationUpdates(provider, 400, 1, (android.location.LocationListener) this);
    }

    private void setupMapMarkerClick(){
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Safety net
                if (AppSession.getInstance(getActivity()).getUser() == null) return false;
                final UserLocation userLocation = markers.get(marker);

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

                alertDialog.setTitle(userLocation.getLocationUser().getUserDisplayName());


                alertDialog.setMessage("Would you like to tag " + userLocation.getLocationUser().getUserDisplayName() + "? Enter a custom tag message below");

                final EditText input = new EditText(getActivity());
                alertDialog.setView(input);

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Tag!", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        pubnubStore.sendTag2User(userLocation.getLocationUser(), input.getText().toString());
                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                    } });

                alertDialog.show();
                return false;
            }
        });
    }

    private void showNameEntryDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton(android.R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setTitle("Your Name Required");
        alert.setMessage("Please enter your name in the box below to use this app.");
        alert.setCancelable(false);

        final EditText input = new EditText(getActivity());
        alert.setView(input);

        alert.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                AlertDialog alertDialog = ((AlertDialog)dialog);

                Button negButton = alert.getButton(Dialog.BUTTON_NEGATIVE);
                if (negButton != null)negButton.setEnabled(false);
                Button positiveButton = alert.getButton(Dialog.BUTTON_POSITIVE);
                if (positiveButton != null)
                positiveButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                       if (input.getText().toString().length() < 3){
                           Toast.makeText(getActivity(), "Please enter name that is longer than 3 characters long.", Toast.LENGTH_SHORT).show();
                       }else{
                           User user = new User();
                           Double randomNumber = (Math.random() * ( 999999 - 1000 ));
                           user.setUserId(randomNumber.intValue());
                           user.setUserDisplayName(input.getText().toString());
                           AppSession.getInstance(getActivity()).setUser(user);
                           alert.dismiss();
                       }
                    }
                });
            }
        });
        alert.show();
    }
}

