package com.appdar.buddytag.buddytag.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sj.singh on 3/14/14.
 */
public class User {
    private Integer mUserId;
    private String mUserDisplayName;

    public Integer getUserId() {
        return mUserId;
    }

    public void setUserId(Integer userId) {
        mUserId = userId;
    }

    public String getUserDisplayName() {
        return mUserDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        mUserDisplayName = userDisplayName;
    }

    public static User userFromJSON(JSONObject json) throws JSONException {
        User user = new User();

        user.setUserId(json.getInt("id"));
        user.setUserDisplayName(json.getString("name"));

        return user;
    }
}
