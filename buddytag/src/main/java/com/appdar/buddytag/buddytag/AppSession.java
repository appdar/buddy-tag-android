package com.appdar.buddytag.buddytag;

import android.content.Context;
import android.content.SharedPreferences;

import com.appdar.buddytag.buddytag.model.User;

/**
 * Created by sj.singh on 3/15/14.
 */
public class AppSession {
    final static String PREF_KEY_USER_ID = "prefs_userId";
    final static String PREF_KEY_USER_NAME = "prefs_userDisplayName";

    User mUser;
    Context mContext;
    private static AppSession ourInstance = new AppSession();

    public static AppSession getInstance(Context context)
    {
        ourInstance.mContext = context;
        return ourInstance;
    }

    private AppSession() {
    }

    //Store the logged in user into SharePreferences.
    //In the real world this would be saved in a secure keychain
    public User getUser() {
        if (mUser == null){
            User user = new User();
            SharedPreferences prefs = mContext.getSharedPreferences("prefs", Context.MODE_PRIVATE);
            Integer userId = prefs.getInt(PREF_KEY_USER_ID, 0);
            user.setUserId(prefs.getInt(PREF_KEY_USER_ID, 0));
            user.setUserDisplayName(prefs.getString(PREF_KEY_USER_NAME, null));
            if (user.getUserId() != 0 && user.getUserDisplayName() != null) mUser = user;
        }
        return mUser;
    }

    public void setUser(User user) {
        SharedPreferences prefs = mContext.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_KEY_USER_ID, user.getUserId());
        editor.putString(PREF_KEY_USER_NAME, user.getUserDisplayName());
        editor.commit();
        mUser = user;
    }
}
