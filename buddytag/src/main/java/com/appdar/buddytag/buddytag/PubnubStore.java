package com.appdar.buddytag.buddytag;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.appdar.buddytag.buddytag.model.User;
import com.appdar.buddytag.buddytag.model.UserLocation;
import com.google.android.gms.maps.model.LatLng;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sj.singh on 3/14/14.
 */

public class PubnubStore {
    final String PUBNUB_CHANNEL_LOCATION = "location_update_channel";
    final String PUBNUB_CHANNEL_TAG = "tagged_channel";

    private Pubnub pubnub;
    private PubnubStoreListener mCallback;
    private Context mContext;
    Boolean isConnected = false;

    public void connect(PubnubStoreListener callback){
        mCallback = callback;
        pubnub = new Pubnub("pub-c-273d4a8f-25ce-4aea-b0d9-a2145418dbb2", "sub-c-862068e4-aaef-11e3-858b-02ee2ddab7fe");
        subscribeChannels();
        pubnub.setResumeOnReconnect(false);
    }

    public void setContext(Context context){
        mContext = context;
    }

    public void unsubscribeChannels(){
        pubnub.unsubscribeAll();
    }

    //Broadcast the user location!
    public void sendUserLocation(double lat, double lng){
        if (!isConnected) return;
        User user = AppSession.getInstance(mContext).getUser();
        if (user == null) return;

        JSONObject payloadJSON = new JSONObject();

        try {
            JSONObject userJSON = new JSONObject();
            JSONObject locationJSON = new JSONObject();
            userJSON.put("id", user.getUserId());
            userJSON.put("name", user.getUserDisplayName());

            locationJSON.put("lat", lat);
            locationJSON.put("lng", lng);

            payloadJSON.put("user", userJSON);
            payloadJSON.put("location", locationJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pubnub.publish(PUBNUB_CHANNEL_LOCATION, payloadJSON, new Callback() {
            public void successCallback(String channel, Object response) {}
            public void errorCallback(String channel, PubnubError error) {}
        });

    }

    //Broadcast that you are tagging a user
    public void sendTag2User(User user, String message){
        if (!isConnected) return;
        User taggedByUser = AppSession.getInstance(mContext).getUser();
        if (taggedByUser == null) return;
        if (user == null) return;

        JSONObject userJSON = new JSONObject();
        JSONObject userTaggedByJSON = new JSONObject();

        JSONObject payloadJSON = new JSONObject();

        try {
            userJSON.put("id", user.getUserId());
            userJSON.put("name", user.getUserDisplayName());

            userTaggedByJSON.put("id", taggedByUser.getUserId());
            userTaggedByJSON.put("name", taggedByUser.getUserDisplayName());

            payloadJSON.put("user", userJSON);
            payloadJSON.put("taggedBy", userTaggedByJSON);
            payloadJSON.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pubnub.publish(PUBNUB_CHANNEL_TAG, payloadJSON, new Callback() {
            public void successCallback(String channel, Object response) {}
            public void errorCallback(String channel, PubnubError error) {}
        });

    }

    //Subscribe to pubnub's location & tag channel
    private void subscribeChannels() {
        try {

            pubnub.subscribe(new String[]{PUBNUB_CHANNEL_LOCATION, PUBNUB_CHANNEL_TAG}, new Callback() {

                        @Override
                        public void connectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : CONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                            isConnected = true;
                        }

                        @Override
                        public void disconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : DISCONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                            isConnected = false;
                        }

                        public void reconnectCallback(String channel, Object message) {
                            Log.d("PUBNUB", "SUBSCRIBE : RECONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                            isConnected = true;
                        }

                        @Override
                        public void successCallback(String channel, Object message) {

                            if (channel.equals(PUBNUB_CHANNEL_LOCATION)) {

                                receivedUserLocation((JSONObject) message);

                            } else if (channel.equals(PUBNUB_CHANNEL_TAG)) {

                                receivedUserTagged((JSONObject) message);

                            }


                        }

                        @Override
                        public void errorCallback(String channel, PubnubError error) {
                            Log.d("PUBNUB", "SUBSCRIBE : ERROR on channel " + channel
                                    + " : " + error.toString());
                            isConnected = false;
                        }
                    },
                    13770170271708028L
            );
        } catch (PubnubException e) {
            Log.d("PUBNUB", e.toString());
            isConnected = false;
        }
    }

    //We have a user that moved so call the interface to update the location
    private void receivedUserLocation(JSONObject jsonObject)  {

        UserLocation userLocation = null;
        User user = null;
        try {
            JSONObject userJSON = jsonObject.getJSONObject("user");
            user = User.userFromJSON(userJSON);
            JSONObject locationJSON = jsonObject.getJSONObject("location");
            userLocation = UserLocation.userLocationFromJSON(locationJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (user == null || userLocation == null) return;

        userLocation.setLocationUser(user);

        mCallback.onLocationUpdate(userLocation);
    }

    //A user was tagged
    //Check to see if the tagged user is the current user logged in
    //if so, call the interface so we can display it on the GUI
    private void receivedUserTagged(JSONObject jsonObject) {
        User currentUser = AppSession.getInstance(mContext).getUser();

        User userTaggedBy = null;
        String tagMessage = null;
        if (currentUser == null) return;
        JSONObject userJSON = null;
        try {
            userJSON = jsonObject.getJSONObject("user");
            User user = User.userFromJSON(userJSON);

            //The tag is not for me!
            if (!user.getUserId().equals(currentUser.getUserId())) return;

            JSONObject userTaggedByJSON = jsonObject.getJSONObject("taggedBy");
            userTaggedBy = User.userFromJSON(userTaggedByJSON);

            tagMessage = jsonObject.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (userTaggedBy == null) return;
        mCallback.onUserTagged(userTaggedBy, tagMessage);
    }

    public interface PubnubStoreListener {
        public void onLocationUpdate(UserLocation userLocation);
        public void onUserTagged(User taggedBy, String tagMessage);
    }
}
