package com.appdar.buddytag.buddytag.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sj.singh on 3/14/14.
 */
public class UserLocation {
    private User mLocationUser;
    private LatLng mLocationLatLng;

    public User getLocationUser() {
        return mLocationUser;
    }

    public void setLocationUser(User locationUser) {
        mLocationUser = locationUser;
    }

    public LatLng getLocationLatLng() {
        return mLocationLatLng;
    }

    public void setLocationLatLng(LatLng location) {
        mLocationLatLng = location;
    }

    public static UserLocation userLocationFromJSON(JSONObject json) throws JSONException {
        UserLocation userLocation = new UserLocation();

        LatLng location = new LatLng(json.getDouble("lat"), json.getDouble("lng"));
        
        userLocation.setLocationLatLng(location);
        return userLocation;
    }

}
